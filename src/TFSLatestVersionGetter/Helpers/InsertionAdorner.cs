﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace TFSLatestVersionGetter.Helpers
{
	public class InsertionAdorner : Adorner
	{
		private static readonly Pen s_pen;
		private static readonly PathGeometry s_triangle;

		#region Fields
		private readonly AdornerLayer m_adornerLayer;
		private readonly bool m_isSeparatorHorizontal;
		#endregion

		// Create the pen and triangle in a static constructor and freeze them to improve performance.

		#region Ctor
		static InsertionAdorner()
		{
			s_pen = new Pen { Brush = Brushes.Gray, Thickness = 2 };
			s_pen.Freeze();

			var firstLine = new LineSegment(new Point(0, -5), false);
			firstLine.Freeze();
			var secondLine = new LineSegment(new Point(0, 5), false);
			secondLine.Freeze();

			var figure = new PathFigure { StartPoint = new Point(5, 0) };
			figure.Segments.Add(firstLine);
			figure.Segments.Add(secondLine);
			figure.Freeze();

			s_triangle = new PathGeometry();
			s_triangle.Figures.Add(figure);
			s_triangle.Freeze();
		}

		public InsertionAdorner(bool isSeparatorHorizontal, bool isInFirstHalf, UIElement adornedElement, AdornerLayer adornerLayer)
			: base(adornedElement)
		{
			m_isSeparatorHorizontal = isSeparatorHorizontal;
			IsInFirstHalf = isInFirstHalf;
			m_adornerLayer = adornerLayer;
			IsHitTestVisible = false;

			m_adornerLayer.Add(this);
		}
		#endregion

		#region Properties
		public bool IsInFirstHalf { get; set; }
		#endregion

		public void Detach()
		{
			m_adornerLayer.Remove(this);
		}

		// This draws one line and two triangles at each end of the line.

		private void DrawTriangle(DrawingContext drawingContext, Point origin, double angle)
		{
			drawingContext.PushTransform(new TranslateTransform(origin.X, origin.Y));
			drawingContext.PushTransform(new RotateTransform(angle));

			drawingContext.DrawGeometry(s_pen.Brush, null, s_triangle);

			drawingContext.Pop();
			drawingContext.Pop();
		}

		private void CalculateStartAndEndPoint(out Point startPoint, out Point endPoint)
		{
			startPoint = new Point();
			endPoint = new Point();

			var width = AdornedElement.RenderSize.Width;
			var height = AdornedElement.RenderSize.Height;

			if (m_isSeparatorHorizontal)
			{
				endPoint.X = width;
				if (!IsInFirstHalf)
				{
					startPoint.Y = height;
					endPoint.Y = height;
				}
			}
			else
			{
				endPoint.Y = height;
				if (!IsInFirstHalf)
				{
					startPoint.X = width;
					endPoint.X = width;
				}
			}
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			Point startPoint;
			Point endPoint;

			CalculateStartAndEndPoint(out startPoint, out endPoint);
			drawingContext.DrawLine(s_pen, startPoint, endPoint);

			if (m_isSeparatorHorizontal)
			{
				DrawTriangle(drawingContext, startPoint, 0);
				DrawTriangle(drawingContext, endPoint, 180);
			}
			else
			{
				DrawTriangle(drawingContext, startPoint, 90);
				DrawTriangle(drawingContext, endPoint, -90);
			}
		}
	}
}