﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace TFSLatestVersionGetter.Helpers
{
	internal static class NativeMethods
	{
		[DllImport("user32.dll")]
		private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

		[DllImport("user32.dll")]
		private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetForegroundWindow(IntPtr hWnd);

		// ReSharper disable InconsistentNaming
		private const int GWL_STYLE = -16;
		private const int WS_MAXIMIZEBOX = 0x10000;
		// ReSharper restore InconsistentNaming

		internal static void DisableMaximizeButton(Window window)
		{
			var hwnd = new WindowInteropHelper(window).Handle;
			var value = GetWindowLong(hwnd, GWL_STYLE);
			SetWindowLong(hwnd, GWL_STYLE, value & ~WS_MAXIMIZEBOX);
		}

		internal static void SetForegroundWindow(Window window)
		{
			var hwnd = new WindowInteropHelper(window).Handle;
			SetForegroundWindow(hwnd);
		}
	}
}
