﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace TFSLatestVersionGetter.Helpers
{
	public class DraggedAdorner : Adorner
	{
		#region Fields
		private readonly AdornerLayer m_adornerLayer;
		private readonly ContentPresenter m_contentPresenter;
		private double m_left;
		private double m_top;
		#endregion

		#region Ctor
		public DraggedAdorner(object dragDropData, DataTemplate dragDropTemplate, UIElement adornedElement, AdornerLayer adornerLayer) : base(adornedElement)
		{
			m_adornerLayer = adornerLayer;

			m_contentPresenter = new ContentPresenter
			{
				Content = dragDropData,
				ContentTemplate = dragDropTemplate,
				//Opacity = 0.7
			};

			m_adornerLayer.Add(this);
		}
		#endregion

		#region Properties
		protected override int VisualChildrenCount
		{
			get { return 1; }
		}
		#endregion

		public void SetPosition(double left, double top)
		{
			// -1 and +13 align the dragged adorner with the dashed rectangle that shows up
			// near the mouse cursor when dragging.
			m_left = left - 1;
			m_top = top + 13;
			if (m_adornerLayer != null)
			{
				try
				{
					m_adornerLayer.Update(AdornedElement);
				}
				catch { }
			}
		}

		public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
		{
			var result = new GeneralTransformGroup();
			result.Children.Add(base.GetDesiredTransform(transform));
			result.Children.Add(new TranslateTransform(m_left, m_top));

			return result;
		}

		public void Detach()
		{
			m_adornerLayer.Remove(this);
		}

		protected override Size MeasureOverride(Size constraint)
		{
			m_contentPresenter.Measure(constraint);
			return m_contentPresenter.DesiredSize;
		}

		protected override Size ArrangeOverride(Size finalSize)
		{
			m_contentPresenter.Arrange(new Rect(finalSize));
			return finalSize;
		}

		protected override Visual GetVisualChild(int index)
		{
			return m_contentPresenter;
		}
	}
}
