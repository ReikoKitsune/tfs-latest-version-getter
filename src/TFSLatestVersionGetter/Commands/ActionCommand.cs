﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace TFSLatestVersionGetter.Commands
{
	/// <summary>
	/// A command whose sole purpose is to 
	/// relay its functionality to other
	/// objects by invoking delegates. The
	/// default return value for the CanExecute
	/// method is 'true'.
	/// </summary>
	public class ActionCommand : ICommand
	{
		private readonly Action m_execute;
		private readonly Func<bool> m_canExecute;

		/// <summary>
		/// Creates a new command that can always execute.
		/// </summary>
		/// <param name="execute">The execution logic.</param>
		public ActionCommand(Action execute) : this(execute, null)
		{
		}

		/// <summary>
		/// Creates a new command.
		/// </summary>
		/// <param name="execute">The execution logic.</param>
		/// <param name="canExecute">The execution status logic.</param>
		public ActionCommand(Action execute, Func<bool> canExecute)
		{
			if (execute == null) throw new ArgumentNullException("execute");

			m_execute = execute;
			m_canExecute = canExecute;
		}

		[DebuggerStepThrough]
		public bool CanExecute(object parameter)
		{
			return m_canExecute == null || m_canExecute();
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				if (m_canExecute != null) CommandManager.RequerySuggested += value;
			}
			remove
			{
				if (m_canExecute != null) CommandManager.RequerySuggested -= value;
			}
		}

		public void Execute(object parameter)
		{
			m_execute();
		}
	}

	/// <summary>
	/// A command whose sole purpose is to 
	/// relay its functionality to other
	/// objects by invoking delegates. The
	/// default return value for the CanExecute
	/// method is 'true'.
	/// </summary>
	public class ActionCommand<T> : ICommand
	{
		private readonly Action<T> m_execute;
		private readonly Predicate<T> m_canExecute;

		public ActionCommand(Action<T> execute) : this(execute, null)
		{
		}

		/// <summary>
		/// Creates a new command.
		/// </summary>
		/// <param name="execute">The execution logic.</param>
		/// <param name="canExecute">The execution status logic.</param>
		public ActionCommand(Action<T> execute, Predicate<T> canExecute)
		{
			if (execute == null) throw new ArgumentNullException("execute");

			m_execute = execute;
			m_canExecute = canExecute;
		}

		[DebuggerStepThrough]
		public bool CanExecute(object parameter)
		{
			return m_canExecute == null || m_canExecute((T)parameter);
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				if (m_canExecute != null) CommandManager.RequerySuggested += value;
			}
			remove
			{
				if (m_canExecute != null) CommandManager.RequerySuggested -= value;
			}
		}

		public void Execute(object parameter)
		{
			m_execute((T)parameter);
		}
	}
}
