﻿using System.Runtime.Serialization;
using TFSLatestVersionGetter.ViewModels;

namespace TFSLatestVersionGetter.Models
{
	[DataContract(Namespace = Namespaces.Main)]
	internal class Folder
	{
		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public string Path { get; set; }

		public Folder(string name, string path)
		{
			Name = name;
			Path = path;
		}

		internal static Folder Create(FolderListBoxItemViewModel vm)
		{
			return new Folder(vm.Name, vm.Path);
		}
	}
}
