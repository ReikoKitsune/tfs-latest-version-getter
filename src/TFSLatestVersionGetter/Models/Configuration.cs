﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows;

namespace TFSLatestVersionGetter.Models
{
	[DataContract(Namespace = Namespaces.Main)]
	internal class Configuration
	{
		[DataMember]
		internal List<Folder> Folders { get; set; }

		[DataMember]
		internal Size MainWindowSize { get; set; }

		public Configuration()
		{
			Folders = new List<Folder>();
		}
	}
}
