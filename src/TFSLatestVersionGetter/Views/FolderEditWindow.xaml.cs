﻿using System.Windows;
using TFSLatestVersionGetter.ViewModels;

namespace TFSLatestVersionGetter.Views
{
	/// <summary>
	/// Interaction logic for FolderEditWindow.xaml
	/// </summary>
	public partial class FolderEditWindow
	{
		public FolderEditWindow()
		{
			InitializeComponent();
			Loaded += (s, e) =>
			{
				FolderNameTextBox.Focus();
				FolderNameTextBox.SelectAll();
			};
		}

		internal FolderEditWindow(FolderEditViewModel viewModel) : this()
		{
			DataContext = viewModel;
		}

		private void SaveButton_OnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void CancelButton_OnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
