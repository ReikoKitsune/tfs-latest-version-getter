﻿using System;
using System.Windows;
using TFSLatestVersionGetter.Helpers;

namespace TFSLatestVersionGetter.Views
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void ShowWindow()
		{
			Show();
			WindowState = WindowState.Normal;
			NativeMethods.SetForegroundWindow(this);
		}

		private void ShowMenuItem_OnClick(object sender, RoutedEventArgs e)
		{
			ShowWindow();
		}

		private void ExitMenuItem_OnClick(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void TaskbarIcon_OnTrayMouseDoubleClick(object sender, RoutedEventArgs e)
		{
			ShowWindow();
		}

		private void MainWindow_OnStateChanged(object sender, EventArgs e)
		{
			if (WindowState == WindowState.Minimized) Hide();
		}
	}
}
