﻿using System;
using System.IO;
using System.Runtime.Serialization;
using TFSLatestVersionGetter.Models;

namespace TFSLatestVersionGetter.Services
{
	internal static class ConfigurationService
	{
		private const string AppName = "ActualBranchKeeper";
		private const string SettingsFileName = "settings.xml";

		public static Configuration Load()
		{
			var filePath = GetSettingsFilePath();
			if (!File.Exists(filePath)) return null;

			try
			{
				var serializer = new DataContractSerializer(typeof(Configuration));
				using (var fs = File.Open(filePath, FileMode.Open))
				{
					return serializer.ReadObject(fs) as Configuration;
				}
			}
			catch
			{
				return null;
			}
		}

		public static void Save(Configuration settings)
		{
			if (settings == null) return;

			var filePath = GetSettingsFilePath();
			var fileInfo = new FileInfo(filePath);

			try
			{
				if (!Directory.Exists(fileInfo.DirectoryName))
				{
					Directory.CreateDirectory(fileInfo.DirectoryName);
				}
				var serializer = new DataContractSerializer(typeof(Configuration));
				using (var fs = File.Open(filePath, FileMode.Create))
				{
					serializer.WriteObject(fs, settings);
				}
			}
			catch
			{
			}
		}

		private static string GetSettingsFilePath()
		{
			var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			return Path.Combine(appData, AppName, SettingsFileName);
		}
	}
}
