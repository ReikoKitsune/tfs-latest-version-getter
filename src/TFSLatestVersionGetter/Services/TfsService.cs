﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace TFSLatestVersionGetter.Services
{
	internal class TfsService
	{
		private static readonly Lazy<TfsService> s_instance = new Lazy<TfsService>(() => new TfsService(), LazyThreadSafetyMode.PublicationOnly);

		public static TfsService Instance
		{
			get { return s_instance.Value; }
		}

		private TfsService()
		{
			var collections = RegisteredTfsConnections.GetProjectCollections();
			if (collections == null || collections.Length == 0) throw new InvalidOperationException();

			TfsTeamProjectCollection = TfsTeamProjectCollectionFactory.GetTeamProjectCollection(collections[0]);
			if (TfsTeamProjectCollection == null) throw new InvalidOperationException();

			VersionControlServer = TfsTeamProjectCollection.GetService<VersionControlServer>();
		}

		internal TfsTeamProjectCollection TfsTeamProjectCollection { get; private set; }

		internal VersionControlServer VersionControlServer { get; private set; }

		public Workspace GetWorkspace(string localFilePath)
		{
			if (string.IsNullOrEmpty(localFilePath)) throw new ArgumentNullException("localFilePath");

			var wsInfo = Workstation.Current.GetLocalWorkspaceInfo(localFilePath);
			return wsInfo == null
				? null
				: VersionControlServer.GetWorkspace(wsInfo);
		}

		public WorkspaceVersionSpec GetWorkspaceVersion(Workspace workspace)
		{
			if (workspace == null) throw new ArgumentNullException("workspace");
			return new WorkspaceVersionSpec(workspace);
		}

		public Changeset GetLastWorkspaceChangeset(Workspace workspace, string path)
		{
			if (workspace == null) throw new ArgumentNullException("workspace");

			var workspaceVersion = GetWorkspaceVersion(workspace);
			var historyParams = new QueryHistoryParameters(path, RecursionType.Full);
			{
				historyParams.ItemVersion = workspaceVersion;
				historyParams.VersionEnd = workspaceVersion;
				historyParams.MaxResults = 1;
			}

			return VersionControlServer.QueryHistory(historyParams).FirstOrDefault();
		}

		public GetStatus GetLatestVersions(string localFilePath)
		{
			if (string.IsNullOrEmpty(localFilePath)) throw new ArgumentNullException("localFilePath");

			var workspace = GetWorkspace(localFilePath);
			if (workspace == null)
			{
				throw new InvalidOperationException(string.Format("Workspace for path: '{0}' is not found!", localFilePath));
			}
			var getRequest = new GetRequest(localFilePath, RecursionType.Full, VersionSpec.Latest);
			return workspace.Get(getRequest, GetOptions.None);
		}

		public Task<GetStatus> GetLatestVersionAsync(string localFilePath)
		{
			var result = new Task<GetStatus>(() => GetLatestVersions(localFilePath));
			result.Start();
			return result;
		}
	}
}
