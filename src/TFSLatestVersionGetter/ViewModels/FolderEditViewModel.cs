﻿using System.Windows.Input;
using TFSLatestVersionGetter.Commands;
using WPFFolderBrowser;

namespace TFSLatestVersionGetter.ViewModels
{
	internal class FolderEditViewModel : ViewModelBase
	{
		private string m_folderName;
		private string m_folderPath;

		internal FolderEditViewModel(string folderName, string folderPath)
		{
			FolderName = folderName;
			FolderPath = folderPath;

			SelectFolder = new ActionCommand(SelectFolderHandler);
		}

		public ICommand SelectFolder { get; set; }

		public string FolderName
		{
			get { return m_folderName; }
			set
			{
				m_folderName = value;
				OnPropertyChanged();
			}
		}

		public string FolderPath
		{
			get { return m_folderPath; }
			set
			{
				m_folderPath = value;
				OnPropertyChanged();
			}
		}

		private void SelectFolderHandler()
		{
			var dialog = new WPFFolderBrowserDialog("Select folder");
			if (dialog.ShowDialog() != true) return;

			FolderPath = dialog.FileName;
		}
	}
}
