﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using TFSLatestVersionGetter.Properties;

namespace TFSLatestVersionGetter.ViewModels
{
	internal abstract class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler == null) return;

			handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
