﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TFSLatestVersionGetter.Commands;
using TFSLatestVersionGetter.Models;
using TFSLatestVersionGetter.Services;
using TFSLatestVersionGetter.Views;

namespace TFSLatestVersionGetter.ViewModels
{
	internal class FolderListBoxItemViewModel : ViewModelBase
	{
		private string m_name;
		private string m_path;
		private Visibility m_pathVisibility = Visibility.Visible;
		private Visibility m_progressBarVisibility = Visibility.Hidden;
		private bool m_isNotUpdating = true;

		internal FolderListBoxItemViewModel(string name, string path)
		{
			Name = name;
			Path = path;

			RenameFolder = new ActionCommand(RenameFolderHandler);
			UpdateFolder = new ActionCommand(() => UpdateFolderHandler());
		}

		public ICommand RenameFolder { get; set; }

		public ICommand UpdateFolder { get; set; }

		public string Name
		{
			get { return m_name; }
			set
			{
				m_name = value;
				OnPropertyChanged();
			}
		}

		public string Path
		{
			get { return m_path; }
			set
			{
				m_path = value;
				OnPropertyChanged();
			}
		}

		public bool IsNotUpdating
		{
			get { return m_isNotUpdating; }
			set
			{
				m_isNotUpdating = value;
				OnPropertyChanged();
			}
		}

		public Visibility PathVisibility
		{
			get { return m_pathVisibility; }
			set
			{
				m_pathVisibility = value;
				OnPropertyChanged();
			}
		}

		public Visibility ProgressBarVisibility
		{
			get { return m_progressBarVisibility; }
			set
			{
				m_progressBarVisibility = value;
				OnPropertyChanged();
			}
		}

		private void RenameFolderHandler()
		{
			var viewModel = new FolderEditViewModel(Name, Path);
			var dialog = new FolderEditWindow(viewModel);
			if (dialog.ShowDialog() == true)
			{
				Name = viewModel.FolderName;
				Path = viewModel.FolderPath;
			}
		}

		internal async Task UpdateFolderHandler()
		{
			try
			{
				IsNotUpdating = false;
				PathVisibility = Visibility.Hidden;
				ProgressBarVisibility = Visibility.Visible;
				await TfsService.Instance.GetLatestVersionAsync(Path);
			}
			catch (Exception ex)
			{
				var errorMessage = string.Format("Get latest version for '{0}' failed with exception:\n{1}", Name, ex.Message);
				MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
			}
			finally
			{
				IsNotUpdating = true;
				PathVisibility = Visibility.Visible;
				ProgressBarVisibility = Visibility.Hidden;
			}
		}

		internal static FolderListBoxItemViewModel Create(Folder folder)
		{
			return new FolderListBoxItemViewModel(folder.Name, folder.Path);
		}
	}
}
