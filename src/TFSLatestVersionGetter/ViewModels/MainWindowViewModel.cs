﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TFSLatestVersionGetter.Commands;
using TFSLatestVersionGetter.Helpers;
using TFSLatestVersionGetter.Models;
using TFSLatestVersionGetter.Services;
using WPFFolderBrowser;

namespace TFSLatestVersionGetter.ViewModels
{
	internal class MainWindowViewModel : ViewModelBase
	{
		private readonly Configuration m_settings = ConfigurationService.Load();
		private bool m_isNotGlobalUpdate = true;

		internal MainWindowViewModel()
		{
			if (m_settings == null) m_settings = new Configuration();
			Folders = new ObservableCollection<FolderListBoxItemViewModel>(m_settings.Folders.Select(FolderListBoxItemViewModel.Create));

			AddFolder = new ActionCommand(AddFolderHandler);
			RemoveFolder = new ActionCommand<FolderListBoxItemViewModel>(RemoveFolderHandler);
			UpdateAllFolders = new ActionCommand(UpdateAllFoldersHandler);

			WindowLoaded = new ActionCommand<Window>(WindowLoadedHandler);
			WindowSizeChanged = new ActionCommand<Window>(WindowSizeChangedHandler);
			WindowClosing = new ActionCommand(WindowClosingHandler);
		}

		public ObservableCollection<FolderListBoxItemViewModel> Folders { get; set; }

		public ICommand AddFolder { get; set; }

		public ICommand RemoveFolder { get; set; }

		public ICommand UpdateAllFolders { get; set; }

		public ICommand WindowLoaded { get; set; }

		public ICommand WindowSizeChanged { get; set; }

		public ICommand WindowClosing { get; set; }

		public bool IsNotGlobalUpdate
		{
			get { return m_isNotGlobalUpdate; }
			set
			{
				m_isNotGlobalUpdate = value;
				OnPropertyChanged();
			}
		}

		private void AddFolderHandler()
		{
			var dialog = new WPFFolderBrowserDialog("Select folder");
			if (dialog.ShowDialog() != true) return;

			var path = dialog.FileName;
			Folders.Add(new FolderListBoxItemViewModel("New Folder", path));
		}

		private void RemoveFolderHandler(FolderListBoxItemViewModel folder)
		{
			if (folder == null) throw new ArgumentNullException("folder");
			Folders.Remove(folder);
		}

		private async void UpdateAllFoldersHandler()
		{
			IsNotGlobalUpdate = false;
			await Task.WhenAll(Folders.Select(folder => folder.UpdateFolderHandler()));
			IsNotGlobalUpdate = true;
		}

		private void WindowLoadedHandler(Window window)
		{
			if (window == null) return;

			NativeMethods.DisableMaximizeButton(window);
			window.Width = m_settings.MainWindowSize.Width;
			window.Height = m_settings.MainWindowSize.Height;
		}

		private void WindowSizeChangedHandler(Window window)
		{
			if (window == null) return;
			if (window.WindowState == WindowState.Minimized) return;

			m_settings.MainWindowSize = new Size(window.Width, window.Height);
		}

		private void WindowClosingHandler()
		{
			m_settings.Folders = Folders.Select(Folder.Create).ToList();
			ConfigurationService.Save(m_settings);
		}
	}
}
